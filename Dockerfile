FROM rust:latest

RUN rustup target add thumbv6m-none-eabi
RUN rustup target add thumbv7m-none-eabi
RUN rustup target add thumbv7em-none-eabi
RUN rustup target add thumbv7em-none-eabihf

RUN apt-get update && apt-get install -y gcc-arm-none-eabi